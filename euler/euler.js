var fs = require("fs");

function euler1() {
    let sum = 0;
    for (var i = 1; i < 1000; i++) {
        if (i % 3 == 0) {
            console.log(i);
            sum = sum + i;
        }
        else if (i % 5 == 0) {
            console.log(i);
            sum = sum + i;
        }
    }
    console.log(sum);

}

function fib(n) {
    if (n <= 0) return 0;
    else if (n == 1) return 1;
    else return fib(n - 1) + fib(n - 2);
}

function euler2() {
    let a = 0;
    let b = 1;
    let sum = 0;
    while (a + b < 4000000) {
        let fib = a + b;
        console.log(fib);
        a = b;
        b = fib;

        if (fib % 2 == 0) {
            sum = sum + fib;
        }
    }
    console.log(sum);
}

function isPrime(n) {
    for (var i = 2; i < n; i++) {
        if (n % i == 0) {
            return false;
        }
    }
    return true;
}

function euler3(n) {
    for (i = Math.floor(Math.sqrt(n)); i > 0; i--) {
        const answer = (x) => {
            console.log("Original number: " + n);
            console.log(x);
        };
        let isFactor = (n % i == 0);
        if (isFactor) {
            if ((n / i) > i) {
                if (isPrime(n / i)) {
                    answer(n / i);
                    break;
                }
            }
            if (isPrime(i)) {
                answer(i);
                break;
            }
        }
    }
}

function isPalindrome(n) {
    let nString = n.toString();
    if (nString.split("").reverse().join("") == nString) {
        return true;
    }
    else {
        return false;
    }
}

function euler4() {
    let ans = 0;
    for (var i = 999; i > 99; i--) {
        for (var j = 999; j > 99; j--) {
            let n = (i * j);
            if (n > ans) {
                if (isPalindrome(n)) {
                    ans = n;
                }
            }
        }
    }
    console.log(ans);
}

function euler5(n) {
    for (var i = 40; i < 2.432902e+18; i++) {

        let goodboi = true;
        for (var j = 2; j <= n; j++) {

            if (i % j !== 0) {
                goodboi = false;
                break;
            }
        }

        //if for loop ended successfuly 
        if (goodboi) {
            return i;
        }
        //otherwise, it goes to next number
        //if tests always test if a condition is true. So if a boolean variable such as goodboi is true, it will return i;  

    }
}

function sumOfSquares(n) {
    let sum = 0;
    for (var i = 1; i <= n; i++) {
        sum = sum + i * i;
    }
    return sum;
}

function squareOfSum(n) {
    let sum = 0;
    for (var i = 1; i <= n; i++) {
        sum = sum + i;
    }
    return (sum * sum);
}

function euler6(n) {
    return squareOfSum(n) - sumOfSquares(n);
}

function euler7(n) {
    let count = 1;
    let i = 1;
    while (count < n) {
        i += 2;
        if (isPrime(i)) {
            count = count + 1;
        }
    }
    return i;
}

const euler8String = "7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450";
const euler8StringArray = Array.from(euler8String);
const euler8NumberArray = euler8StringArray.map(x => parseInt(x));

function euler8(n) {
    let maxProduct = 0;
    for (var i = 0; i < euler8NumberArray.length - n; i++) {
        let product = 1;
        for (var j = i; j < i + n; j++) {
            product = product * euler8NumberArray[j];
        }
        if (product > maxProduct) {
            maxProduct = product;
        }
    }
    return maxProduct;
}

function euler9(n) {
    for (var a = 1; a < n - 1; a++) {
        for (var b = a + 1; b < n; b++) {
            for (var c = b + 1; c < n; c++) {
                if (a + b + c == n) {
                    if ((a * a) + (b * b) == (c * c)) {
                        return a * b * c;
                    }
                }
            }
        }
    }
}

function euler10(n) {
    let sum = 2;
    for (var i = 3; i < n; i += 2) {
        if (isPrime(i)) {
            sum = sum + i;
        }
    }
    return sum;
}

class GridCalc {
    constructor(numCols, length) {
        this.numCols = numCols;
        this.length = length;
    }
    at(array, index) {
        if (index > -1 && index < this.length) {
            return array[index];
        }
        else {
            return 0;
        }
    }
    n(i) {
        if (i == -1) return -1;
        if (this.firstRow(i)) return -1;
        return i - this.numCols;
    }
    s(i) {
        if (i == -1) return -1;
        if (this.lastRow(i)) return -1;
        return i + this.numCols;
    }
    e(i) {
        if (i == -1) return -1;
        if (this.lastColumn(i)) return -1;
        return i + 1;
    }
    w(i) {
        if (i == -1) return -1;
        if (this.firstColumn(i)) return -1;
        return i - 1;
    }
    ne(i) {
        if (i == -1) return -1;
        if (this.firstRow(i) || this.lastColumn(i)) return -1;
        return i - (this.numCols - 1);
    }
    se(i) {
        if (i == -1) return -1;
        if (this.lastRow(i) || this.lastColumn(i)) return -1;
        return i + (this.numCols + 1);
    }
    sw(i) {
        if (i == -1) return -1;
        if (this.firstColumn(i) || this.lastRow(i)) return -1;
        return i + (this.numCols - 1);
    }
    nw(i) {
        if (i == -1) return -1;
        if (this.firstRow(i) || this.firstColumn(i)) return -1;
        return i - (this.numCols + 1);
    }
    firstRow(i) {
        return ((i >= 0) && (i < this.numCols));
    }
    lastRow(i) {
        return ((i >= 0) && (i + this.numCols >= this.length));
    }
    firstColumn(i) {
        return ((i >= 0) && (i % this.numCols == 0));
    }
    lastColumn(i) {
        return ((i >= 0) && ((i + 1) % this.numCols == 0));
    }
}

const euler11String = "08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08 49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00 81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65 52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91 22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80 24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50 32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70 67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21 24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72 21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95 78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92 16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57 86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58 19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40 04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66 88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69 04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36 20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16 20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54 01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48";
const euler11StringArray = euler11String.split(" ");
const euler11NumberArray = euler11StringArray.map(x => parseInt(x));

function euler11() {
    let maxProduct = 0;
    let arr = euler11NumberArray;
    const gc = new GridCalc(20, 400);

    for (var i = 0; i <= arr.length - 4; i++) {
        let product = 1;
        product = gc.at(arr, i)
            * gc.at(arr, gc.s(i))
            * gc.at(arr, gc.s(gc.s(i)))
            * gc.at(arr, gc.s(gc.s(gc.s(i))));
        maxProduct = Math.max(product, maxProduct);

        product = gc.at(arr, i)
            * gc.at(arr, gc.e(i))
            * gc.at(arr, gc.e(gc.e(i)))
            * gc.at(arr, gc.e(gc.e(gc.e(i))));
        maxProduct = Math.max(product, maxProduct);

        product = gc.at(arr, i)
            * gc.at(arr, gc.se(i))
            * gc.at(arr, gc.se(gc.se(i)))
            * gc.at(arr, gc.se(gc.se(gc.se(i))));
        maxProduct = Math.max(product, maxProduct);

        product = gc.at(arr, i)
            * gc.at(arr, gc.sw(i))
            * gc.at(arr, gc.sw(gc.sw(i)))
            * gc.at(arr, gc.sw(gc.sw(gc.sw(i))));
        maxProduct = Math.max(product, maxProduct);
    }
    return maxProduct;
}

function testGridCalc() {

    const gridCalc = new GridCalc(3, 9);

    console.assert(gridCalc.n(0) == -1, "n");

    console.assert(gridCalc.firstRow(1), "firstRow 1");
    console.assert(gridCalc.firstRow(5) == false, "firstRow 5");

    console.assert(gridCalc.lastRow(7), "lastRow 7");
    console.assert(gridCalc.lastRow(2) == false, "lastRow 2");

    console.assert(gridCalc.firstColumn(3), "firstColumn 3");
    console.assert(gridCalc.firstColumn(4) == false, "firstColumn 4");

    console.assert(gridCalc.lastColumn(8), "lastColumn 8");
    console.assert(gridCalc.lastColumn(1) == false, "lastColumn 1");
}
//testGridCalc();

function numDivisors(n) {
    let numDivisors = 0;
    for (var i = Math.floor(Math.sqrt(n)); i > 0; i--) {
        if (n % i == 0) {
            numDivisors += 1;
        }
    }
    return numDivisors * 2;
}

function euler12(n) {
    let triangle = 0;
    for (var i = 1; true; i++) {
        triangle += i;
        if (numDivisors(triangle) > n) {
            return triangle;
        }
    }
}

function readBigInts(datafile) {
    var text = fs.readFileSync(datafile).toString();
    var textByLine = text.split("\n");
    const bigIntArray = textByLine.map(x => BigInt(x));
    return bigIntArray;
}

function euler13(array) {
    let sum = 0n;
    for (var i = 0; i < array.length; i++) {
        sum = sum + array[i];
    }
    return (sum.toString().substring(0, 10));
}

function euler14(n) {
    let maxCount = 0;
    let startingNumber = 0;
    for (var i = n; i > 0; i--) {
        let count = 1;
        let j = i;
        while (j > 1) {
            if (j % 2 == 0) {
                j = (j / 2);
            }
            else {
                j = ((3 * j) + 1);
            }
            count += 1;
        }
        if (count > maxCount) {
            maxCount = count;
            startingNumber = i;
        }
        // console.log(i + ": " + count);
    }
    return startingNumber;
}

function factorialOf(n) {
    let bigN = BigInt(n);
    let product = bigN;
    for (var i = bigN - 1n; i >= 1; i--) {
        product = product * i;
    }
    return product;
}

function euler15(n) {
    return factorialOf((n * 2)) / ((factorialOf(n) * factorialOf(n)));
}

function euler16Calc() {
    const euler16String = (2n ** 1000n).toString();
    const euler16StringArray = Array.from(euler16String);
    const euler16NumberArray = euler16StringArray.map(x => parseInt(x));

    let sum = 0;
    for (var i = 0; i < euler16NumberArray.length; i++) {
        sum += euler16NumberArray[i];
    }
    return sum;
}

function toWords(n) {
    console.assert(n >= 0 && n <= 1000, "n >= 0 and <= 1000");
    if (n <= 20) {
        switch (n) {
            case 0: return "zero";
            case 1: return "one";
            case 2: return "two";
            case 3: return "three";
            case 4: return "four";
            case 5: return "five";
            case 6: return "six";
            case 7: return "seven";
            case 8: return "eight";
            case 9: return "nine";
            case 10: return "ten";
            case 11: return "eleven";
            case 12: return "twelve";
            case 13: return "thirteen";
            case 14: return "fourteen";
            case 15: return "fifteen";
            case 16: return "sixteen";
            case 17: return "seventeen";
            case 18: return "eighteen";
            case 19: return "nineteen";
            case 20: return "twenty";
        }
    }

    if (n == 30) return "thirty";
    if (n < 30) {
        let s = "twenty";
        if (n > 20) {
            s += " ";
            s += toWords(n - 20);
            return s;
        }
    }

    if (n == 40) return "forty";
    if (n < 40) {
        let s = "thirty";
        if (n > 30) {
            s += " ";
            s += toWords(n - 30);
            return s;
        }
    }

    if (n == 50) return "fifty";
    if (n < 50) {
        let s = "forty";
        if (n > 40) {
            s += " ";
            s += toWords(n - 40);
            return s;
        }
    }

    if (n == 60) return "sixty";
    if (n < 60) {
        let s = "fifty";
        if (n > 50) {
            s += " ";
            s += toWords(n - 50);
            return s;
        }
    }

    if (n == 70) return "seventy";
    if (n < 70) {
        let s = "sixty";
        if (n > 60) {
            s += " ";
            s += toWords(n - 60);
            return s;
        }
    }

    if (n == 80) return "eighty";
    if (n < 80) {
        let s = "seventy";
        if (n > 70) {
            s += " ";
            s += toWords(n - 70);
            return s;
        }
    }

    if (n == 90) return "ninety";
    if (n < 90) {
        let s = "eighty";
        if (n > 80) {
            s += " ";
            s += toWords(n - 80);
            return s;
        }
    }
    if (n < 100) {
        let s = "ninety";
        if (n > 90) {
            s += " ";
            s += toWords(n - 90);
            return s;
        }
    }

    if (n == 100) return "one hundred";
    if (n < 200) {
        let s = "one hundred and";
        if (n > 100) {
            s += " ";
            s += toWords(n - 100);
            return s;
        }
    }

    if (n == 200) return "two hundred";
    if (n < 300) {
        let s = "two hundred and";
        if (n > 200) {
            s += " ";
            s += toWords(n - 200);
            return s;
        }
    }

    if (n == 300) return "three hundred";
    if (n < 400) {
        let s = "three hundred and";
        if (n > 300) {
            s += " ";
            s += toWords(n - 300);
            return s;
        }
    }

    if (n == 400) return "four hundred";
    if (n < 500) {
        let s = "four hundred and";
        if (n > 400) {
            s += " ";
            s += toWords(n - 400);
            return s;
        }
    }

    if (n == 500) return "five hundred";
    if (n < 600) {
        let s = "five hundred and";
        if (n > 500) {
            s += " ";
            s += toWords(n - 500);
            return s;
        }
    }

    if (n == 600) return "six hundred";
    if (n < 700) {
        let s = "six hundred and";
        if (n > 600) {
            s += " ";
            s += toWords(n - 600);
            return s;
        }
    }

    if (n == 700) return "seven hundred";
    if (n < 800) {
        let s = "seven hundred and";
        if (n > 700) {
            s += " ";
            s += toWords(n - 700);
            return s;
        }
    }

    if (n == 800) return "eight hundred";
    if (n < 900) {
        let s = "eight hundred and";
        if (n > 800) {
            s += " ";
            s += toWords(n - 800);
            return s;
        }
    }

    if (n == 900) return "nine hundred";
    if (n < 1000) {
        let s = "nine hundred and";
        if (n > 900) {
            s += " ";
            s += toWords(n - 900);
            return s;
        }
    }
    if (n == 1000) {
        return "one thousand";
    }
}

function removeSpaces(s) {
    console.assert(typeof s == "string", "typeof s is a string");
    if (typeof s != "string") {
        throw new Error(s);
    }

    const regex = / /g;
    return s.replace(regex, "");
}

function euler17() {
    let count = 0;
    for (var i = 1; i <= 1000; i++) {

        count += removeSpaces(toWords(i)).length;
    }
    return count;
}

const adjacentTo = (r, i) => [[r + 1, i], [r + 1, i + 1]];
//console.log(adjacentTo(0, 0));

const euler18Array = [
    [75],
    [95, 64],
    [17, 47, 82],
    [18, 35, 87, 10],
    [20, 4, 82, 47, 65],
    [19, 1, 23, 75, 3, 34],
    [88, 2, 77, 73, 7, 63, 67],
    [99, 65, 4, 28, 6, 16, 70, 92],
    [41, 41, 26, 56, 83, 40, 80, 70, 33],
    [41, 48, 72, 33, 47, 32, 37, 16, 94, 29],
    [53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14],
    [70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57],
    [91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48],
    [63, 66, 4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31],
    [4, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 4, 23]
];

function val(r, i) {
    return euler18Array[r][i];
}

function max(a, b) {
    if (a > b) return a;
    else return b;
}

function euler18(r, i) {
    if (r == euler18Array.length) return 0;
    return max(euler18(r + 1, i), euler18(r + 1, i + 1)) + val(r, i);
}

function euler19() {
    let numSundays = 0;
    let dow = 1;
    for (var year = 1901; year <= 2000; year++) {

        for (var day = 1; day <= 366; day++) {

            dow++;
            if (dow == 7) dow = 0;

            // testing using specific case to see if there are any other bugs. Will make into a general case later
            if (year % 4 == 0) {
                if (dow == 0 && (day == 1 ||
                    day == 32 ||
                    day == 61 ||
                    day == 92 ||
                    day == 122 ||
                    day == 153 ||
                    day == 183 ||
                    day == 214 ||
                    day == 244 ||
                    day == 275 ||
                    day == 305 ||
                    day == 336)) numSundays += 1;
            }
            else if (dow == 0 && (day == 1 ||
                day == 32 ||
                day == 60 ||
                day == 91 ||
                day == 121 ||
                day == 152 ||
                day == 182 ||
                day == 213 ||
                day == 243 ||
                day == 274 ||
                day == 304 ||
                day == 335)) {
                numSundays += 1;
                if (day == 365) break;
            }
        }
    }
    return numSundays;
}

function euler20(n) {
    const euler16String = (factorialOf(n)).toString();
    const euler16StringArray = Array.from(euler16String);
    const euler16NumberArray = euler16StringArray.map(x => parseInt(x));

    let sum = 0;
    for (var i = 0; i < euler16NumberArray.length; i++) {
        sum += euler16NumberArray[i];
    }
    return sum;
}

function sumOfPDivisors(n) {
    let sum = 0;
    for (var i = 1; i <= Math.floor(n / 2); i++) {
        if (n % i == 0) sum += i;
    }
    return sum;
}

function isAmicable(a) {
    let b = sumOfPDivisors(a);
    if (sumOfPDivisors(b) == a) return true;
    else return false;
}


function euler21(n) {
    // pre-compute sum of proper divisors for every number
    let d = [];
    for (var i = 1; i < n; i++) {
        d[i] = sumOfPDivisors(i);
    }

    //finds amicable numbers between 1 and n and adds them to the set.
    let amicableNumbers = new Set();
    for (var a = 1; a < n; a++) {
        let b = d[a];
        if (a !== b && d[b] == a) {
            amicableNumbers.add(a);
            amicableNumbers.add(b);
        }
    }

    //return sum of set
    let sum = 0;
    for (let item of amicableNumbers) {
        sum += item;
        //console.log(item);
    }
    return sum;
}

function readNames(datafile) {
    function removeQuotes(s) {
        const regex = /"/gi;
        return s.replace(regex, "");
    }

    var text = fs.readFileSync(datafile).toString();
    var textSplit = text.split(",");
    const namesArray = textSplit.map(x => removeQuotes(x));
    return namesArray;
}

function sort(namesArray) {
    let sortedArray = [];

    for (var j = 0; j < namesArray.length; j++) {
        let lowestName = "zzzzzzzzzzzzzzz";
        let lowestNameIndex = 0;

        for (var i = 0; i < namesArray.length; i++) {
            if (namesArray[i] < lowestName) {
                lowestName = namesArray[i];
                lowestNameIndex = i;

            }
        }

        sortedArray.push(lowestName);
        namesArray[lowestNameIndex] = 9999;

    }
    return sortedArray;
}

function alphaValue(s) {
    let sum = 0;
    for (var i = 0; i < s.length; i++) {
        sum += (s.charCodeAt(i) - 64);
    }
    return sum;
}

function euler22() {
    let a = readNames("p022_names.txt");
    let b = sort(a);

    let total = 0;
    for (var i = 0; i < b.length; i++) {
        total += (i + 1) * alphaValue(b[i]);
    }
    return total;
}

console.log(euler22());




