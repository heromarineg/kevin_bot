VERSION_FILE = ./src/version.ts
VERSION := $(shell sed -n "1s|.*\"\(.*\)\".*|\1|p" $(VERSION_FILE))
REGISTRY_USERNAME = anticrisis
BUILT_IMAGE = kevin_bot
REPOSITORY = kevin_bot
REPOSITORY_TAG = v$(VERSION)

.PHONY: usage
.DEFAULT: usage

usage:
	@echo Version: $(VERSION)
	@echo Tag: $(REPOSITORY_TAG)

all: build tag push

dist/index.js: 
	$(shell rm dist/index.js)
	tsc

build-ts: dist/index.js

build: build-ts
	@echo 
	@echo Building...
	docker build -t $(BUILT_IMAGE) .

tag:
	@echo
	@echo Tagging...
	docker tag $(BUILT_IMAGE) $(REGISTRY_USERNAME)/$(REPOSITORY):latest
	docker tag $(BUILT_IMAGE) $(REGISTRY_USERNAME)/$(REPOSITORY):$(REPOSITORY_TAG)

push:
	@echo
	@echo Pushing...
	docker push $(REGISTRY_USERNAME)/$(REPOSITORY):$(REPOSITORY_TAG)
	docker push $(REGISTRY_USERNAME)/$(REPOSITORY):latest