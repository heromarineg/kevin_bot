import readline from "readline";

class Hangman {
    constructor(
        public guesses: string = "",
        public answer: string = "",
        public lastGuess: string = "",
    ) { }

    wordGenerator() {
        this.answer = "example";
    }
    hangmanInit() {

        this.wordGenerator();
        const letters = this.answer.split("");

        console.log("Welcome fellow oompaloompas! Let's play hangman.");
        console.log("I will think of a " + 5 + " letter word, you try and guess it.");
        console.log(this.answer);
        console.log(letters);
    }
}


function hangmanMessage(msg: string) {

}








const gHangman = new Hangman();


const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: "> "
});

rl.prompt();

rl.on("line", line => {
    hangmanMessage(line);
    rl.prompt();

    if (line === "k!hangman") {
        gHangman.hangmanInit();
    }
});

rl.on("close", () => {
    console.log("closing");
    process.exit(0);
});


