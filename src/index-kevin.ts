import * as fs from "fs";
import * as dotenv from "dotenv";
import { Client, Message } from "discord.js";

dotenv.config();

/* TO DO LIST:
    -Add social features(Tatsumaki ex.)
    -Keyword tts // msg.channel.send("I can see you hahaa...", {tts: true});
    -Hydration bot
    -Stretch bot
*/

const HELP = `
Greetings fleshy mammal, let us do normal person things like normal hoomans do!
\`\`\`
k!guessinggame to play a 1-100 guessing game with me

k!duel to play a 1v1 guessing game with another player

k!partyguess to play a multiplayer 1-100 guessing game 

k!stats to view your total points and number of different kevin games played

And if you want some company, I'll be listening... :)
\`\`\`
`;

const DATAFILE = "./data/data.json";

const client = new Client();

function getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

function isFromBot(msg: Message) {
    return msg.author.id === client.user.id;
}

class PlayerRecord {
    constructor(
        public duelsPlayed: number = 0,
        public guessingGamesPlayed: number = 0,
        public partyGuessesPlayed: number = 0,
        public totalPoints: number = 0
    ) { }

    endDuelGame() {

    }

    endGuessingGame() {

    }

    endPartyGuess() {

    }

    addPoints(n: number) {
        this.totalPoints = this.totalPoints + n;

    }
}

class DuelGame {
    constructor(
        public player1: string = "",
        public player2: string = "",
        public player1Guess: number = -1,
        public player2Guess: number = -1
    ) { }

    reset() {
        this.player1 = "";
        this.player2 = "";
        this.player1Guess - 1;
        this.player2Guess = -1;
    }
}

class GuessingGame {
    constructor(
        public user: string = "",
        public userGuess: number = -1,
        public answer: number = -1,
        public sum: number = -1
    ) { }

    start(msg: Message) {
        this.sum = 1;
        this.user = msg.author.id;
        this.answer = getRandomInt(1, 101);
    }

    reset() {
        this.user = "";
        this.userGuess = -1;
        this.answer = -1;
        this.sum = -1;
    }
}

class PartyGuess {
    constructor(
        public winner: string = "",
        public answer: number = -1,
        public players: string[] = [],
        public leader: string = "",
        public begun: boolean = false
    ) { }

    start(msg: Message) {
        this.answer = getRandomInt(1, 101);
        this.leader = msg.author.id;
        this.players.push(this.leader);
    }

    reset() {
        this.winner = "";
        this.players = [];
        this.answer = -1;
        this.begun = false;
    }
}

class HydrationBot {
    constructor(
        public users: string = "",
        public userOnline: boolean = false,
        public userMobile: boolean = true,
        public userTimer: number = 0
    ) { }

    addUser() {
        // subscribe user to hydration bot messages
    }

    removeUser() {
        // remove user from hydration bot messages 
    }

    timer() {
        //
    }

    isUserOnline() {

    }

    isUserMobile() {

    }
}

class StretchBot { }

class ChannelState {
    constructor(
        public currentGame: string = "",
        public duelGame: DuelGame = new DuelGame(),
        public guessingGame: GuessingGame = new GuessingGame(),
        public partyGuess: PartyGuess = new PartyGuess()
    ) { }

    reset() {
        this.currentGame = "";
    }

    inDuelGame() {
        return this.currentGame === "duel";
    }

    inGuessingGame() {
        return this.currentGame === "guessinggame";
    }

    inPartyGuess() {
        return this.currentGame === "partyguess";
    }

    startGame(name: string, msg: Message) {
        if (name === "duel") {
            this.currentGame = name;
            msg.channel.send("Hello users, please type Player1 and Player2 to join. Guess a number between 1-10 and whoever gets closer wins!");
        }
        else if (name === "guessinggame") {
            this.currentGame = name;
            this.guessingGame.start(msg);
            msg.channel.send("I will pick a number between 1-100, try to guess it!");

        }
        else if (name === "partyguess") {
            this.currentGame = name;
            this.partyGuess.start(msg);
            msg.channel.send("I will pick a number between 1-100, anyone who wants to play, type 'join' Once everyone is ready, the leader can type 'start'");
        }
    }
}

class BotState {
    constructor(
        public channels: Map<string, ChannelState> = new Map(),
        private playerRecords: Map<string, PlayerRecord> = new Map()
    ) { }

    getOrCreateChannelState(id: string): ChannelState {
        if (this.channels.has(id)) {
            const r = this.channels.get(id);
            return r ? r : new ChannelState();
        }
        else {
            const cs = new ChannelState();
            this.channels.set(id, cs);
            return cs;
        }
    }

    getOrCreatePlayerRecord(id: string): PlayerRecord {
        if (this.playerRecords.has(id)) {
            const r = this.playerRecords.get(id);
            return r ? r : new PlayerRecord();
        }
        else {
            const cs = new PlayerRecord();
            this.playerRecords.set(id, cs);
            return cs;
        }
    }

    setPlayerRecord(id: string, r: PlayerRecord) {
        this.playerRecords.set(id, r);
    }

    dtoGetPlayerRecords = () => this.playerRecords;
}

// -- default bot state -----------------------------------------------------
// will be reassigned by "ready" code which reads from file
let gBotState = new BotState();

// --------------------------------------------------------------------------

function partyGuessMessage(msg: Message, channelState: ChannelState) {
    const command = msg.content.toLowerCase();
    const partyGuess = channelState.partyGuess;
    const i = parseInt(command) || -1;

    if (!isFromBot(msg)) {
        if (command === "k!cancel") {
            if (msg.author.id === partyGuess.leader) {
                partyGuess.reset();
                channelState.reset();
                msg.channel.send("Game has been canceled :)");
            }
            else {
                msg.reply("You are simply a mere mortal, only the supreme leader may cancel the game.");
            }
        }

        if (command === "join") {
            if (partyGuess.players.includes(msg.author.id)) {
                msg.channel.send("You've already joined buddy");
            }
            else {
                if (partyGuess.begun === false) {
                    partyGuess.players.push(msg.author.id);
                    msg.channel.send("Welcome, " + msg.author.tag);
                }
                else {
                    msg.reply("You're too late! The game has already started");
                }
            }
        }

        if (command === "start") {
            if (msg.author.id === partyGuess.leader) {
                partyGuess.begun = true;
                msg.channel.send("Let the guessing begin!");
            }
            else if (msg.author.id !== partyGuess.leader) {
                msg.channel.send("You are simply a mere mortal, only the supreme leader may start the game");
            }
            else if (partyGuess.players.length === 1) {
                msg.channel.send("Get some friends, buddy. You can't play with youself :P");
            }
        }
        if (i !== -1) {
            if (partyGuess.begun === true) {
                if (partyGuess.players.includes(msg.author.id)) {
                    if (i !== partyGuess.answer) {
                        if (i > partyGuess.answer) {
                            msg.channel.send(i + " is too large");
                        }
                        else if (i < partyGuess.answer) {
                            msg.channel.send(i + " is too small");
                        }
                    }
                    else {

                        partyGuess.winner = msg.author.id;

                        let pr = gBotState.getOrCreatePlayerRecord(partyGuess.winner);
                        pr.addPoints(partyGuess.players.length);
                        msg.channel.send("The number was " + partyGuess.answer);
                        msg.channel.send(msg.author.tag + " has won " + partyGuess.players.length + " points!");

                        for (const p of partyGuess.players) {
                            let pr1 = gBotState.getOrCreatePlayerRecord(p);
                            pr1.partyGuessesPlayed = pr1.partyGuessesPlayed + 1;
                        }

                        partyGuess.reset();
                        channelState.reset();
                    }
                }
                else {
                    msg.reply("Stop trolling :P You're not in the game");
                }
            }
            else {
                msg.channel.send("Hold your horses, the game hasn't started yet");
            }
        }
    }
}

function duelGameMessage(msg: Message, channelState: ChannelState) {
    const command = msg.content.toLowerCase();
    const duelGame = channelState.duelGame;

    if (command === "k!cancel") {
        if (msg.author.id === duelGame.player1 || msg.author.id === duelGame.player2) {
            duelGame.reset();
            channelState.reset();
            msg.channel.send("Game has been canceled :)");
        }
        else {
            msg.reply("Stop trolling :P");
        }
    }
    else if (command === "player1") {
        if (duelGame.player1 === "") {
            duelGame.player1 = msg.author.id;
            if (duelGame.player1 === duelGame.player2) {
                duelGame.reset();
                channelState.reset();
                msg.channel.send("You can't play with yourself, buddy");
            }
            else {
                msg.channel.send("Welcome, Player 1, please type in your guess :)");
            }
        }
        else {
            msg.reply("There can only be 1 player1 :P");
        }
    }
    else if (command === "player2") {
        if (duelGame.player2 === "") {
            duelGame.player2 = msg.author.id;
            if (duelGame.player2 === duelGame.player1) {
                duelGame.reset();
                channelState.reset();
                msg.channel.send("You can't play with yourself, buddy");
                msg.channel.send("Bot state has been reset");
            }
            else {
                msg.channel.send("Welcome, Player 2, please type in your guess :D");
            }
        }
        else {
            msg.reply("There can only be 1 player2 :P");
        }
    }
    else {
        const i = parseInt(command) || -1;
        if (i !== -1) {
            if (msg.author.id === duelGame.player1) {
                duelGame.player1Guess = i;
                msg.channel.send("Player 1's guess is " + duelGame.player1Guess);
            }
            if (msg.author.id === duelGame.player2) {
                duelGame.player2Guess = i;
                msg.channel.send("Player 2's guess is " + duelGame.player2Guess);
            }
        }
    }
    const n = getRandomInt(1, 11);

    if (duelGame.player1Guess >= 11) {
        msg.channel.send("That's higher than 10, goofball!");
        duelGame.player1Guess = -1;
    }
    else if (duelGame.player2Guess >= 11) {
        msg.channel.send("That's higher than 10, goofball!");
        duelGame.player2Guess = -1;
    }
    /*else if (duelGame.player1Guess < 1) {
        msg.channel.send("That's less than 1, goofball!");
        duelGame.player1Guess = -1;
    }
    else if (duelGame.player2Guess < 1) {
        msg.channel.send("that's less than 1, goofball!");
        duelGame.player2Guess = -1;
    } */

    if (duelGame.player1Guess !== -1 && duelGame.player2Guess !== -1) {
        if (duelGame.player1Guess === duelGame.player2Guess) {

            msg.channel.send("Guess again, dummies, you cant guess the same number :P");

            duelGame.player1Guess = -1;
            duelGame.player2Guess = -1;
        }
        else if (Math.abs(duelGame.player1Guess - n) < Math.abs(duelGame.player2Guess - n)) {

            let pr1 = gBotState.getOrCreatePlayerRecord(duelGame.player1);
            let pr2 = gBotState.getOrCreatePlayerRecord(duelGame.player2);

            msg.channel.send("Player 1 wins!");
            msg.channel.send("The number was " + n);

            pr1.addPoints(3);
            if (pr2.totalPoints !== 0) {
                pr2.addPoints(-1);
            }
            pr1.duelsPlayed = pr2.duelsPlayed + 1;
            pr2.duelsPlayed = pr1.duelsPlayed + 1;

            gBotState.setPlayerRecord(duelGame.player1, pr1);

            gBotState.setPlayerRecord(duelGame.player2, pr2);

            if (pr2.totalPoints !== 0) {
                msg.channel.send("Player 1 wins 3 points! You now have " + pr1.totalPoints + " points");
                msg.channel.send("Player 2 lost 1 point! You now have " + pr2.totalPoints + " points");
            }
            else if (pr2.totalPoints === 0) {
                msg.channel.send("Player 1 wins 3 points! You now have " + pr1.totalPoints + " points");
                msg.channel.send("Player 2 has 0 points, so i'll be nice and not subtract any :)");
            }

            duelGame.reset();
            channelState.reset();
        }
        else if (Math.abs(duelGame.player1Guess - n) == Math.abs(duelGame.player2Guess - n)) {

            msg.channel.send("It's a tie!");
            msg.channel.send("The number was " + n);

            duelGame.reset();
            channelState.reset();
        }
        else {
            let pr2 = gBotState.getOrCreatePlayerRecord(duelGame.player2);
            let pr1 = gBotState.getOrCreatePlayerRecord(duelGame.player1);

            msg.channel.send("Player 2 wins!");
            msg.channel.send("The number was " + n);

            pr2.addPoints(3);
            if (pr1.totalPoints !== 0) {
                pr1.addPoints(-1);
            }

            pr2.duelsPlayed = pr2.duelsPlayed + 1;
            pr1.duelsPlayed = pr1.duelsPlayed + 1;

            gBotState.setPlayerRecord(duelGame.player2, pr2);
            gBotState.setPlayerRecord(duelGame.player1, pr1);

            if (pr1.totalPoints !== 0) {
                msg.channel.send("Player 2 wins 3 points! You now have " + pr1.totalPoints + " points");
                msg.channel.send("Player 1 lost 1 point! You now have " + pr2.totalPoints + " points");
            }
            else if (pr1.totalPoints === 0) {
                msg.channel.send("Player 2 wins 3 points! You now have " + pr1.totalPoints + " points");
                msg.channel.send("Player 1 has 0 points, so i'll be nice and not subtract any :)");
            }

            duelGame.reset();
            channelState.reset();
        }
    }
}

function guessingGameMessage(msg: Message, channelState: ChannelState) {
    const command = msg.content.toLowerCase();
    const guessingGame = channelState.guessingGame;
    const i = parseInt(command) || -1;

    if (command === "k!cancel") {
        if (msg.author.id === guessingGame.user) {
            guessingGame.reset();
            channelState.reset();
            msg.channel.send("Game has been canceled :)");
        }
        else {
            msg.reply("Stop trolling :P");
        }
    }
    if (i !== -1) {
        if (msg.author.id === guessingGame.user) {
            guessingGame.userGuess = i;
            if (guessingGame.userGuess !== guessingGame.answer) {
                if (guessingGame.userGuess > guessingGame.answer) {
                    msg.channel.send("Your guess is too large");
                }
                else if (guessingGame.userGuess < guessingGame.answer) {
                    msg.channel.send("Your guess is too small");
                }

                guessingGame.userGuess = i;
                guessingGame.sum = guessingGame.sum + 1;
            }
        }
        else {
            msg.reply("Wait your turn, kid");
        }
        if (guessingGame.userGuess === guessingGame.answer) {
            msg.channel.send("You got it! You are a smart cookie :)");
            msg.channel.send("You guessed " + guessingGame.sum + " time(s)");

            let pr = gBotState.getOrCreatePlayerRecord(msg.author.id);

            let points = 0;

            switch (guessingGame.sum) {
                case 1:
                    points = 7;
                    break;
                case 2:
                    points = 6;
                    break;
                case 3:
                    points = 5;
                    break;
                case 4:
                    points = 4;
                    break;
                case 5:
                    points = 3;
                    break;
                case 6:
                    points = 2;
                    break;
                case 7:
                    points = 1;
                    break;
                default:
                    points = 0;
                    break;
            }

            pr.addPoints(points);
            pr.guessingGamesPlayed = pr.guessingGamesPlayed + 1;

            gBotState.setPlayerRecord(msg.author.id, pr);

            msg.channel.send("You gained " + points + " points :)");
            msg.channel.send("You have " + pr.totalPoints + " points!");

            guessingGame.reset();
            channelState.reset();
        }


        // ...
    }
}

// -- start of persistence -------------------------------------------------------

function mapToArray<K, V>(m: Map<K, V>, mapper: any = null) {
    return mapper
        ? Array.from(m.entries()).map(([k, v]) => [k, mapper(v)])
        : Array.from(m.entries());
}

function mapFromArray<K, V>(a: [K, V][], mapper: any = null) {
    return mapper
        ? new Map(a.map(([k, v]) => [k, mapper(v)]))
        : new Map(a);
}

class PlayerRecordDto {
    constructor(
        public readonly duelsPlayed = 0,
        public readonly guessingGamesPlayed = 0,
        public readonly partyGuessesPlayed: number = 0,
        public readonly totalPoints = 0
    ) { }

    static fromDomain(o: PlayerRecord) {
        return new PlayerRecordDto(
            o.duelsPlayed,
            o.guessingGamesPlayed,
            o.partyGuessesPlayed,
            o.totalPoints
        );
    }
    static toDomain(dto: PlayerRecordDto) {
        return new PlayerRecord(
            dto.duelsPlayed,
            dto.guessingGamesPlayed,
            dto.partyGuessesPlayed,
            dto.totalPoints
        );
    }
}

class BotStateDto {
    public readonly playerRecords: [string, object][] = [];

    constructor(o: any) {
        this.playerRecords = o.playerRecords ? o.playerRecords : [];
    }

    static fromDomain(o: BotState) {
        return new BotStateDto({ playerRecords: mapToArray(o.dtoGetPlayerRecords(), PlayerRecordDto.fromDomain) });
    }

    static toDomain(dto: BotStateDto) {
        return new BotState(
            new Map(),
            dto.playerRecords ? mapFromArray(dto.playerRecords, PlayerRecordDto.toDomain) : new Map());
    }

    toDomain() {
        return BotStateDto.toDomain(this);
    }
}

class DataFile {
    private static readFileSync(path: string) {
        try {
            return new BotStateDto(
                JSON.parse(fs.readFileSync(path, { encoding: "utf8" })))
                .toDomain();
        }
        catch (err) {
            console.log(`ERROR: readFileSync: ${err}`);
            return new BotState();
        }
    }

    private static writeFileSync(path: string, o: BotState) {
        const js = BotStateDto.fromDomain(o);
        try {
            fs.writeFileSync(path, JSON.stringify(js, null, 2));
        }
        catch (err) {
            console.log(`ERROR: writeFileSync: ${err}`);
        }
    }

    public static readData(path: string) {
        const d = Date.now();
        const bakPath = path + "." + d.toString() + ".json";
        fs.copyFileSync(path, bakPath);
        return DataFile.readFileSync(bakPath);
    }

    public static writeData(path: string, o: BotState) {
        DataFile.writeFileSync(path, o);
    }
}

// -- end of persistence  ----------------------------------------------------


//when add new game dont forget to change strings on both functions

function stats(msg: Message) {
    const pr = gBotState.getOrCreatePlayerRecord(msg.author.id);
    msg.channel.send(`${msg.author.tag}, You have ${pr.totalPoints} points!`);
    msg.channel.send("You've played " + pr.guessingGamesPlayed + " guessing games, " + pr.duelsPlayed + " duels, and " + pr.partyGuessesPlayed + " party guessing games!");
}

function statsMention(msg: Message) {
    for (const [id, user] of msg.mentions.users) {
        const pr = gBotState.getOrCreatePlayerRecord(id);

        msg.channel.send(`${user.tag} has ${pr.totalPoints} points!`);
        msg.channel.send("And has played " + pr.guessingGamesPlayed + " guessing games, " + pr.duelsPlayed + " duels, and " + pr.partyGuessesPlayed + " party guessing games!");
    }
}

function doStats(msg: Message) {
    if (msg.mentions.users.size === 0) {
        stats(msg);

    }
    else {
        statsMention(msg);
    }
}



client.on("ready", (): void => {
    console.log(`DATAFILE = ${DATAFILE}`);
    gBotState = DataFile.readData(DATAFILE);
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on("message", (msg): void => {

    const channelState = gBotState.getOrCreateChannelState(msg.channel.id);

    if (channelState.inDuelGame()) {
        duelGameMessage(msg, channelState);
    }
    else if (channelState.inGuessingGame()) {
        guessingGameMessage(msg, channelState);
    }
    else if (channelState.inPartyGuess()) {
        partyGuessMessage(msg, channelState);
    }
    else {
        const words = msg.content.trim().toLowerCase().split(/\s+/);

        const content = msg.content.trim().toLowerCase();

        if (words.length < 1)
            return;

        const command = words[0];

        if (command === "k!duel") {
            channelState.startGame("duel", msg);
        }
        else if (command === "k!guessinggame") {
            channelState.startGame("guessinggame", msg);
        }
        else if (command === "k!partyguess") {
            channelState.startGame("partyguess", msg);
        }
        else if (content === "hi kevin" || content === "hello kevin" || content === "hey kevin" || content === "hai kevin" || content === "hi, kevin" || content === "hello, kevin" || content === "hey, kevin" || content === "hai, kevin") {
            msg.reply("hi there :)");
        }
        else if (command === "k!stats") {
            doStats(msg);
        }
        else if (command === "k!help") {
            msg.channel.send(HELP);
        }
        else if (content.includes("funny")) {
            msg.channel.send("I can see you hahahahhaa", { tts: true });
        }

        else if (command === "k!new") {
            msg.channel.send("I HAVE EVOLVED MUAHAHAHA! We can now play a party guessing game together! Type ```k!help``` and or ```k!partyguess``` to play!");
        }
        else if (!isFromBot(msg)) {
            if (command === "goodnight" || command === "gn" || command === "Goodnight" || command === "good night" || command === "Good night") {
                msg.channel.send("Goodnight, " + msg.author.tag);
            }
            else if (command === ":)" || command === ":>" || command === ":d") {
                msg.channel.send(":)");
            }
            else if (command === ":(" || command === ":<") {
                msg.channel.send(":(");
            }

        }
    }

    // write botstate back to disk
    DataFile.writeData(DATAFILE, gBotState);

});

client.login(process.env.DISCORD_TOKEN);
