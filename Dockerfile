FROM node:12-alpine

WORKDIR /usr/src/app

ENV DISCORD_TOKEN=

COPY package*.json ./

RUN npm ci --only=production

COPY dist/*.js ./dist/

CMD ["node", "dist/index.js"]
